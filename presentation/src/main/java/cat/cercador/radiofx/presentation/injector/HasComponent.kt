package cat.cercador.radiofx.presentation.injector


interface HasComponent<out C> {
    fun getComponent(): C
}