package cat.cercador.radiofx.presentation.view.activity

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View


class NoSwipeableViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean = false

    override fun onTouchEvent(ev: MotionEvent?): Boolean = false

    override fun executeKeyEvent(event: KeyEvent): Boolean {
        var handled = false
        if (event?.action == KeyEvent.ACTION_DOWN) {
            when (event.keyCode) {
                KeyEvent.KEYCODE_DPAD_LEFT -> null
                KeyEvent.KEYCODE_DPAD_RIGHT -> null
                KeyEvent.KEYCODE_TAB -> {
                    if (KeyEvent.metaStateHasNoModifiers(event.keyCode)) {
                        handled = arrowScroll(View.FOCUS_FORWARD)
                    } else if (KeyEvent.metaStateHasNoModifiers(event.keyCode)) {
                        handled = arrowScroll(View.FOCUS_BACKWARD)
                    }
                }
            }
        }
        return handled
    }
}