package cat.cercador.radiofx.presentation.view.home

import android.content.Context
import cat.cercador.radiofx.presentation.base.BasePresenter
import cat.cercador.radiofx.presentation.base.BaseView
import cat.cercador.radiofx.presentation.model.RadioModel

interface HomeContract {
    interface View : BaseView<Presenter> {
        fun setItems(models: List<RadioModel>)
        fun showMessage()
//        fun weekEventsOK(eventList: ArrayList<ComicModel>)
//        fun weekEventError(error: Error?)

    }

    interface Presenter : BasePresenter<Context, HomeContract.View> {
//        fun getWeekEvents(startDate: String, endDate: String, doesntShowHours: Boolean?, doesntShowInterviews: Boolean?, subjetGuid : String?, groupGuid: String?, version: String?, page: Int?, pageSize : Int?)
        fun getRadioList()
    }

}