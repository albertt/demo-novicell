package cat.cercador.radiofx.presentation.view.customViews

import android.content.Context
import android.support.v4.widget.SwipeRefreshLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import cat.cercador.radiofx.presentation.R


class SwipeEmptyViewComponent : SwipeRefreshLayout {

    private var imageView: ImageView? = null
    private var titleTv: TextView? = null
    private var subtitleTv: TextView? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    private fun init() {
        setColorSchemeResources(R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimary,
                R.color.colorAccent)
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.generic_empty_view, this, true)
        imageView = findViewById(R.id.generic_empty_view_iv) as ImageView
        titleTv = findViewById(R.id.generic_empty_view_title_tv) as TextView
        subtitleTv = findViewById(R.id.generic_empty_view_text_tv) as TextView
    }

//    fun fillViews(emptyViewModel: EmptyViewModel) {
//        imageView!!.setImageDrawable(ContextCompat.getDrawable(context, emptyViewModel.imageId))
//        titleTv!!.text = context.getString(emptyViewModel.title)
//        subtitleTv!!.text = context.getString(emptyViewModel.subtitle)
//    }
}
