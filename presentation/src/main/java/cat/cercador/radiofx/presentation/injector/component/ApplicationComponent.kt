package cat.cercador.radiofx.presentation.injector.component

import cat.cercador.radiofx.presentation.injector.module.ApplicationModule
import cat.cercador.radiofx.presentation.injector.module.SessionModule
import cat.cercador.radiofx.presentation.view.activity.BaseActivity
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [(ApplicationModule::class)])
interface ApplicationComponent {

    fun inject(baseActivity: BaseActivity)

    fun plus(sessionModule: SessionModule): SessionComponent

}