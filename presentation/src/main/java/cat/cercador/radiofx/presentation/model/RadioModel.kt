package cat.cercador.radiofx.presentation.model

import android.os.Parcel
import android.os.Parcelable


data class RadioModel(val id: Int = 0,
                      val name: String = "",
                      val language: String = "",
                      val url: String = "",
                      val active: Boolean = false) : Parcelable {
    constructor(source: Parcel) : this(
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readString(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeString(name)
        writeString(language)
        writeString(url)
        writeInt((if (active) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<RadioModel> = object : Parcelable.Creator<RadioModel> {
            override fun createFromParcel(source: Parcel): RadioModel = RadioModel(source)
            override fun newArray(size: Int): Array<RadioModel?> = arrayOfNulls(size)
        }
    }
}