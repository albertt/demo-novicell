package cat.cercador.radiofx.presentation.utils

object Constants {
    val CURRENT_FRAGMENT_TAG = "CurrentFragmentTAG"
    val CURRENT_SAVED_FRAGMENT = "CurrentSavedFragment"
}