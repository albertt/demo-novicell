package cat.cercador.radiofx.presentation.view.control.interfaces

interface HomeFragmentAdapterListOnClickListener {
    interface AdapterListener : AdapterListOnClickListener.AdapterListener {
        fun onListButtonClicked(position: Int)
        fun onListInterviewCancelButtonClicked(position: Int)
    }

    interface ViewListener : AdapterListOnClickListener.ViewListener {
        fun onListButtonClicked(position: Int)
        fun onListInterviewCancelButtonClicked(position: Int)
    }
}