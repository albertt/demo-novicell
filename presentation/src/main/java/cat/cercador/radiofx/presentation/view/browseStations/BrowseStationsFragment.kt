package cat.cercador.radiofx.presentation.view.browseStations

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cat.cercador.radiofx.domain.entity.User
import cat.cercador.radiofx.presentation.R
import cat.cercador.radiofx.presentation.RadioFXApplication
import cat.cercador.radiofx.presentation.injector.module.BaseFragmentModule
import cat.cercador.radiofx.presentation.injector.module.BaseListModule
import cat.cercador.radiofx.presentation.injector.module.HomeModule
import cat.cercador.radiofx.presentation.model.RadioModel
import cat.cercador.radiofx.presentation.view.control.adapter.HomeAdapter
import cat.cercador.radiofx.presentation.view.control.interfaces.HomeFragmentAdapterListOnClickListener
import cat.cercador.radiofx.presentation.view.control.interfaces.MainActivityFragmentListener
import cat.cercador.radiofx.presentation.view.fragment.BaseFragment
import cat.cercador.radiofx.presentation.view.home.HomeFragment
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*
import javax.inject.Inject

class BrowseStationsFragment : BaseFragment() {

    private lateinit var baseActivityFragmentListener: MainActivityFragmentListener
    private var models: ArrayList<RadioModel>? = null
    private lateinit var homeAdapter: HomeAdapter

    companion object {
        fun newInstance() = BrowseStationsFragment()
        const val TAG = "BrowseStationsFragment"
    }

    override fun getFragmentTag(): String = TAG

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("NewFragment:", TAG)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_browse_stations, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        models = ArrayList<RadioModel>()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivityFragmentListener) {
            baseActivityFragmentListener = context
        }
    }

    private fun setViewItems() {
        homeRecyclerView.layoutManager = LinearLayoutManager(activity)
        if (models != null && context != null) {
            homeRecyclerView.adapter = homeAdapter
        }
    }

    private fun isModelListEmpty() {
        if (models!!.isEmpty()) {
            homeRecyclerView.visibility = View.GONE
            homeEmptyView.visibility = View.VISIBLE
        } else {
            homeRecyclerView.visibility = View.VISIBLE
            homeEmptyView.visibility = View.GONE
        }
    }

    override fun setupFragmentComponent() {

    }
}
