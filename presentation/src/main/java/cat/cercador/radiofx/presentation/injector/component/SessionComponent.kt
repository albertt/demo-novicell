package cat.cercador.radiofx.presentation.injector.component

import cat.cercador.radiofx.presentation.injector.PerSession
import cat.cercador.radiofx.presentation.injector.module.BaseFragmentModule
import cat.cercador.radiofx.presentation.injector.module.BaseListModule
import cat.cercador.radiofx.presentation.injector.module.HomeModule
import cat.cercador.radiofx.presentation.injector.module.SessionModule
import dagger.Subcomponent

@PerSession
@Subcomponent(modules = [SessionModule::class])
interface SessionComponent {

    fun plus(baseFragmentModule: BaseFragmentModule, baseListModule: BaseListModule, homeModule: HomeModule): HomeComponent

}