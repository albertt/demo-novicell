package cat.cercador.radiofx.presentation.injector.component

import cat.cercador.radiofx.presentation.injector.PerFragment
import cat.cercador.radiofx.presentation.injector.module.BaseFragmentModule
import cat.cercador.radiofx.presentation.injector.module.BaseListModule
import cat.cercador.radiofx.presentation.injector.module.HomeModule
import cat.cercador.radiofx.presentation.view.home.HomeFragment
import dagger.Subcomponent

@PerFragment
@Subcomponent(modules = [BaseFragmentModule::class, BaseListModule::class, HomeModule::class])
interface HomeComponent {

    fun inject(homeFragment: HomeFragment)

}