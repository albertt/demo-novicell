package cat.cercador.radiofx.presentation.view.activity

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import cat.cercador.radiofx.presentation.R
import cat.cercador.radiofx.presentation.utils.Constants
import cat.cercador.radiofx.presentation.view.browseStations.BrowseStationsFragment
import cat.cercador.radiofx.presentation.view.control.adapter.FadePageTransformer
import cat.cercador.radiofx.presentation.view.control.adapter.NavBarHomeAdapter
import cat.cercador.radiofx.presentation.view.control.interfaces.MainActivityFragmentListener
import cat.cercador.radiofx.presentation.view.home.HomeFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainActivityFragmentListener {

    private lateinit var adapter: NavBarHomeAdapter
    private var currentPageTag: String = ""

    protected var currentTag: String? = null
    protected var currentFragment: Fragment? = null

    companion object {
        private val REQUEST_PROFILE_SCREEN: Int = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
        setContentView(R.layout.activity_main)
        bottomNavigationBar.inflateMenu(R.menu.bottom_navigation_main)
        setFirstToolbarTitle()
        setSupportActionBar(toolbar)
        setViewPager()
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        supportFragmentManager.putFragment(outState, Constants.CURRENT_SAVED_FRAGMENT, currentFragment)
        outState?.putString(Constants.CURRENT_FRAGMENT_TAG, currentTag)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_activity_menu, menu)
        toolbar.setOnMenuItemClickListener { item ->
            when(item.itemId) {
                R.id.search -> {

                }
                R.id.cast -> {

                }
                R.id.profile -> {
                    openProfile()
                }
            }
            true
        }
        return super.onCreateOptionsMenu(menu)
    }

    private fun setFirstToolbarTitle() {
        toolbar.title = ""
        currentPageTag = HomeFragment.TAG
        mainToolbarTitleTextView.text = "Home"
    }

    private fun setViewPager() {
        setupViewPager()

        bottomNavigationBar.setOnNavigationItemSelectedListener { item ->
            when(item.itemId) {
                R.id.action_favorites -> {
                    mainViewPager.currentItem = 0
                    mainToolbarTitleTextView.text = "Home"
                }
                R.id.action_schedules -> {
                    mainViewPager.currentItem = 1
                    mainToolbarTitleTextView.text = "Browse Stations"
                }
                R.id.action_schedules_dummy -> {
                    mainViewPager.currentItem = 2
                    mainToolbarTitleTextView.text = "Browse Stations"
                }
            }
            true
        }
    }

    private fun setupViewPager() {
        mainViewPager.setPageTransformer(false, FadePageTransformer())
        adapter = NavBarHomeAdapter(supportFragmentManager)
        adapter.addFragment(newHomeFragment(), HomeFragment.TAG)
        adapter.addFragment(newBrowseStationsFragment(), BrowseStationsFragment.TAG)
        adapter.addFragment(newBrowseStationsFragment(), BrowseStationsFragment.TAG)
        mainViewPager.adapter = adapter
    }

    private fun newHomeFragment() = HomeFragment.newInstance()

    private fun newBrowseStationsFragment(): BrowseStationsFragment {
        val browseStationsFragment = BrowseStationsFragment.newInstance()
        return browseStationsFragment
    }

    private fun openProfile() {
        //TODO
    }

    override fun setToolbarTitleFrom(title: String, callingFragmentTag: String) {
        mainToolbarTitleTextView.text = title
    }
}

