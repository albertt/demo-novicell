package cat.cercador.radiofx.presentation.injector.module

import android.content.Context
import cat.cercador.radiofx.data.net.ApiConstants
import cat.cercador.radiofx.data.net.RadioFXApiService
import cat.cercador.radiofx.data.net.interceptor.AuthInterceptor
import cat.cercador.radiofx.data.repository.RadioRepositoryImpl
import cat.cercador.radiofx.data.repository.datasource.RadioDataStore
import cat.cercador.radiofx.data.repository.datasource.RadioDataStoreImpl
import cat.cercador.radiofx.domain.repository.RadioRepository
import cat.cercador.radiofx.presentation.RadioFXApplication
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class ApplicationModule(private val application: RadioFXApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideRadioDataStore(radioDataStoreImpl: RadioDataStoreImpl): RadioDataStore {
        return radioDataStoreImpl
    }

    @Provides
    @Singleton
    fun provideRadioRepository(radioRepositoryImpl: RadioRepositoryImpl): RadioRepository {
        return radioRepositoryImpl
    }

    @Provides
    @Singleton
    fun provideRadioFXApi(authInterceptor: AuthInterceptor): RadioFXApiService {
        val httpClient = OkHttpClient.Builder()
                .addInterceptor(authInterceptor)
                .connectTimeout(ApiConstants.TIMEOUT_CONNECTION_VALUE, TimeUnit.SECONDS)
                .readTimeout(ApiConstants.TIMEOUT_READ_VALUE, TimeUnit.SECONDS)
                .writeTimeout(ApiConstants.TIMEOUT_WRITE_VALUE, TimeUnit.SECONDS)
        val builder = Retrofit.Builder()
                .baseUrl(ApiConstants.ENDPOINT)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
        return builder.client(httpClient.build()).build().create(RadioFXApiService::class.java)
    }

}