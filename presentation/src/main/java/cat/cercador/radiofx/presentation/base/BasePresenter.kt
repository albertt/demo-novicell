package cat.cercador.radiofx.presentation.base

interface BasePresenter<in T, in V> {
    fun start()
}