package cat.cercador.radiofx.presentation.injector.module

import cat.cercador.radiofx.domain.entity.User
import cat.cercador.radiofx.presentation.injector.PerSession
import dagger.Module
import dagger.Provides

@Module
class SessionModule(private val user: User) {

    @Provides
    @PerSession
    fun provideUser(): User {
        return user
    }
}