package cat.cercador.radiofx.presentation.view.home

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cat.cercador.radiofx.domain.entity.User
import cat.cercador.radiofx.presentation.R
import cat.cercador.radiofx.presentation.RadioFXApplication
import cat.cercador.radiofx.presentation.injector.module.BaseFragmentModule
import cat.cercador.radiofx.presentation.injector.module.BaseListModule
import cat.cercador.radiofx.presentation.injector.module.HomeModule
import cat.cercador.radiofx.presentation.model.RadioModel
import cat.cercador.radiofx.presentation.view.browseStations.BrowseStationsFragment
import cat.cercador.radiofx.presentation.view.control.adapter.HomeAdapter
import cat.cercador.radiofx.presentation.view.control.interfaces.HomeFragmentAdapterListOnClickListener
import cat.cercador.radiofx.presentation.view.control.interfaces.MainActivityFragmentListener
import cat.cercador.radiofx.presentation.view.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*
import javax.inject.Inject

class HomeFragment : BaseFragment(), HomeContract.View, HomeFragmentAdapterListOnClickListener.ViewListener {
    @Inject
    lateinit var presenter: HomePresenter
    @Inject
    lateinit var user: User
    private lateinit var baseActivityFragmentListener: MainActivityFragmentListener
    private var models: ArrayList<RadioModel>? = null
    private lateinit var homeAdapter: HomeAdapter

    companion object {
        fun newInstance() = HomeFragment()
        const val TAG = "HomeFragment"
    }

    override fun getFragmentTag(): String = BrowseStationsFragment.TAG

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("NewFragment:", TAG)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        models = ArrayList<RadioModel>()
//        setViewItems()
        presenter.getRadioList()
//        isModelListEmpty()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivityFragmentListener) {
            baseActivityFragmentListener = context
        }
    }

    private fun setViewItems() {
        homeRecyclerView.layoutManager = LinearLayoutManager(activity)
        if (models != null && context != null) {
            homeAdapter = HomeAdapter(models!!, this, context!!)
            homeRecyclerView.adapter = homeAdapter
//            models?.forEachIndexed { index, event ->
//                DateUtils.isCurrentEvent(event)
//                if (event.isCurrentSession!!) {
//                    homeRecyclerView.layoutManager.scrollToPosition(index)
//                    return
//                }
//            }
        }
//        button.text = hello
//        button.setOnClickListener {
//            presenter.getComicListByHero(1011334)
//        }
    }

    override fun setItems(models: List<RadioModel>) {
        models.forEach({
            this.models?.add(it)
        })
        setViewItems()
    }

    private fun isModelListEmpty() {
        if (models!!.isEmpty()) {
            homeRecyclerView.visibility = View.GONE
//            homeEmptyView.fillViews(EmptyViewModel.EMPTY_AGENDA)
            homeEmptyView.visibility = View.VISIBLE
        } else {
            homeRecyclerView.visibility = View.VISIBLE
            homeEmptyView.visibility = View.GONE
        }
    }

    override fun showMessage() {

    }

    override fun onListButtonClicked(position: Int) {

    }

    override fun onListInterviewCancelButtonClicked(position: Int) {

    }

    override fun onItemSelected(position: Int) {

    }

    override fun onItemSelectedToEvaluate(position: Int) {

    }

    override fun onSelectedItems(selectedItems: SparseBooleanArray) {

    }

    override fun setupFragmentComponent() {
        RadioFXApplication
                .sessionComponent!!
                .plus(BaseFragmentModule(context!!), BaseListModule(context!!), HomeModule(this))
                .inject(this)
    }
}
