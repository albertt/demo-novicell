package cat.cercador.radiofx.presentation.view.control.adapter

import android.support.v4.view.ViewPager
import android.view.View


class FadePageTransformer : ViewPager.PageTransformer {
    override fun transformPage(page: View, position: Float) {
        if (position <= -1.0f || position >= 1.0f) {
            page!!.translationX = page.width * position
            page.alpha = 0.0f
        } else if (position == 0.0f) {
            page!!.translationX = page.width * position
            page.alpha = 1.0f
        } else {
            // position is between -1.0F & 0.0F OR 0.0F & 1.0F
            page!!.translationX = page.width * -position
            page.alpha = 1.0f - Math.abs(position)
        }
    }
}