package cat.cercador.radiofx.presentation.injector.module

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import cat.cercador.radiofx.presentation.injector.PerFragment
import dagger.Module
import dagger.Provides

@Module
class BaseListModule(private val context: Context) {

    @Provides
    @PerFragment
    fun provideLinearLayoutManager(context: Context): LinearLayoutManager {
        return LinearLayoutManager(context)
    }
}