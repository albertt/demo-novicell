package cat.cercador.radiofx.presentation.model.mapper

import cat.cercador.radiofx.domain.entity.Radio
import cat.cercador.radiofx.presentation.model.RadioModel


object RadioModelMapper {

    fun toModel(bO: Radio): RadioModel {
        return RadioModel(bO.id, bO.name, bO.language, bO.url, bO.active)
    }

    fun toModel(bOL: Collection<Radio>?): List<RadioModel> {
        val modelList = ArrayList<RadioModel>()
        if (bOL != null && bOL.isNotEmpty()) {
            bOL.forEach {
                modelList.add(toModel(it))
            }
        }
        return modelList
    }

}