package cat.cercador.radiofx.presentation

import android.app.Application
import cat.cercador.radiofx.domain.entity.User
import cat.cercador.radiofx.presentation.injector.component.ApplicationComponent
import cat.cercador.radiofx.presentation.injector.component.DaggerApplicationComponent
import cat.cercador.radiofx.presentation.injector.component.SessionComponent
import cat.cercador.radiofx.presentation.injector.module.ApplicationModule
import cat.cercador.radiofx.presentation.injector.module.SessionModule


class RadioFXApplication : Application() {

    companion object {
        lateinit var instance : RadioFXApplication
        @JvmStatic
        lateinit var applicationComponent: ApplicationComponent
        var sessionComponent: SessionComponent? = null
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initializeInjector()

        //MarvelComicCollectorApplication.instance.createSessionComponent(User("", 0, userEditText.text.toString(), ""))
        createSessionComponent(User("", 0, "nickname", ""))
    }

    private fun initializeInjector() {
        applicationComponent = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
    }

    fun createSessionComponent(user: User) {
        sessionComponent = applicationComponent.plus(SessionModule(user))
    }

    fun releaseUserComponent(){
        sessionComponent = null
    }
}