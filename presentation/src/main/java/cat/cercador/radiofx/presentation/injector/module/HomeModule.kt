package cat.cercador.radiofx.presentation.injector.module

import cat.cercador.radiofx.presentation.injector.PerFragment
import cat.cercador.radiofx.presentation.view.home.HomeContract
import cat.cercador.radiofx.presentation.view.home.HomeFragment
import dagger.Module
import dagger.Provides

@Module
class HomeModule(private val homeFragment: HomeFragment){

    @Provides
    @PerFragment
    fun provideHomeView(): HomeContract.View {
        return homeFragment
    }
}
