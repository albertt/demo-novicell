package cat.cercador.radiofx.presentation.view.home

import cat.cercador.radiofx.domain.entity.Radio
import cat.cercador.radiofx.domain.interactor.GetRadioListUseCase
import cat.cercador.radiofx.domain.interactor.RadioUseCase
import cat.cercador.radiofx.presentation.injector.PerFragment
import cat.cercador.radiofx.presentation.model.RadioModel
import cat.cercador.radiofx.presentation.model.mapper.RadioModelMapper
import javax.inject.Inject

@PerFragment
class HomePresenter @Inject constructor(private val useCase: GetRadioListUseCase) : HomeContract.Presenter {

    @Inject
    lateinit var view: HomeContract.View

    lateinit var models: List<RadioModel>

    override fun getRadioList() {
        useCase.execute(object: RadioUseCase.GetRadioListCallback {
            override fun onRadioListReceived(radioList: List<Radio>) {
                models = RadioModelMapper.toModel(radioList)
                setItems()
            }

            override fun onError() {
                showMessage()
            }
        })
    }

    private fun setItems() {
        view.setItems(models)
    }

    private fun showMessage() {
        view.showMessage()
    }

    override fun start() {
    }
}