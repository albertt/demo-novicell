package cat.cercador.radiofx.presentation.view.control.interfaces

import android.util.SparseBooleanArray

interface AdapterListOnClickListener {
    interface AdapterListener {
        fun onItemSelected(position: Int)
        fun onItemSelectedToEvaluate(position: Int)
    }

    interface ViewListener {
        fun onItemSelected(position: Int)
        fun onItemSelectedToEvaluate(position: Int)
        fun onSelectedItems(selectedItems : SparseBooleanArray)
    }
}