package cat.cercador.radiofx.presentation.injector.module

import android.app.Activity
import cat.cercador.radiofx.presentation.injector.PerActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    @PerActivity
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    @PerActivity
    fun provideHelloWorld(): String {
        return "Hello World"
    }

}