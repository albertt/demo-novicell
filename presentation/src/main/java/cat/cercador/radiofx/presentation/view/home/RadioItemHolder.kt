package cat.cercador.radiofx.presentation.view.home

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import cat.cercador.radiofx.presentation.model.RadioModel
import cat.cercador.radiofx.presentation.view.control.interfaces.AdapterListOnClickListener
import kotlinx.android.synthetic.main.list_item_radio.view.*


class RadioItemHolder(itemView: View?, val adapterListener: AdapterListOnClickListener.AdapterListener, val context: Context) : RecyclerView.ViewHolder(itemView) {

    private lateinit var radio: RadioModel

    fun bindEvent(radio: RadioModel) {
        //TODO manage eventType 1 and 2
        itemView.layoutContainer.setOnClickListener { adapterListener.onItemSelected(adapterPosition) }
        itemView.stationTitleTextView.text = radio.name
        itemView.languageTextView.text = if (radio.language == null) "-" else radio.language
//        itemView.hourTextView.text = "${event.startHour}${Constants.SPACE}${context.getString(R.string.events_from_time_to_time)}${Constants.SPACE}${event.endHour}"
        itemView.urlTextView.text = radio.url
//        itemView.incidentsCountTextView.text = event.incidentsCount.toString()
//        itemView.duttiesCountTextView.text = event.duttiesCount.toString()
//        itemView.activitiesCountTextView.text = event.activitiesCount.toString()
//        itemView.anotationsCountTextView.text = event.annotationsCount.toString()
        this.radio = radio
    }
}