package cat.cercador.radiofx.presentation.view.control.interfaces

interface MainActivityFragmentListener {
    fun setToolbarTitleFrom(title: String, callingFragmentTag: String)
}