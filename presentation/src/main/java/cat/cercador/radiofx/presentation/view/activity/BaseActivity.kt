package cat.cercador.radiofx.presentation.view.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import cat.cercador.radiofx.presentation.R
import cat.cercador.radiofx.presentation.RadioFXApplication
import cat.cercador.radiofx.presentation.injector.component.ApplicationComponent
import cat.cercador.radiofx.presentation.injector.module.ActivityModule
import cat.cercador.radiofx.presentation.view.fragment.BaseFragment
import kotlinx.android.synthetic.main.activity_base.*

abstract class BaseActivity : AppCompatActivity() {

    protected var currentTag: String? = null
    protected var currentFragment: Fragment? = null

    companion object {
        private const val CURRENT_FRAGMENT_TAG: String = "currentTag"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getApplicationComponent().inject(this)
        initializeFragmentAndTAG(savedInstanceState)
        setContentView(R.layout.base_activity_fragment)
        beginTransaction()
    }


    private fun initializeFragmentAndTAG(savedInstanceState: Bundle?) {
        if (savedInstanceState == null || !savedInstanceState.containsKey(CURRENT_FRAGMENT_TAG)) {
            createFragmentAndSettingTAG()
        } else {
            currentTag = savedInstanceState.getString(CURRENT_FRAGMENT_TAG)
            currentFragment = supportFragmentManager.getFragment(savedInstanceState, currentTag)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        if (currentFragment == null) {
            supportFragmentManager.putFragment(outState, currentTag, currentFragment)
            outState?.putString(CURRENT_FRAGMENT_TAG, currentTag)
        }
        super.onSaveInstanceState(outState)
    }

    private fun beginTransaction() {
        if (currentFragment == null) {
            createFragmentAndSettingTAG()
        }
        if (currentFragment != null && fragmentContainer != null) {
            supportFragmentManager.beginTransaction().replace(fragmentContainer.id, currentFragment, (currentFragment as BaseFragment).getFragmentTag()).commit()
        }
    }

    abstract fun createFragmentAndSettingTAG()

    protected fun getApplicationComponent(): ApplicationComponent {
        return RadioFXApplication.applicationComponent
    }

    protected fun getActivityModule(): ActivityModule {
        return ActivityModule(this)
    }

}
