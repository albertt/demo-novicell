package cat.cercador.radiofx.presentation.view.control.adapter


import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import cat.cercador.radiofx.presentation.R
import cat.cercador.radiofx.presentation.model.RadioModel
import cat.cercador.radiofx.presentation.view.control.interfaces.HomeFragmentAdapterListOnClickListener
import cat.cercador.radiofx.presentation.view.home.RadioItemHolder
import java.util.*


class HomeAdapter(private val radioList: ArrayList<RadioModel>, private val viewListener: HomeFragmentAdapterListOnClickListener.ViewListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), HomeFragmentAdapterListOnClickListener.AdapterListener {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val view = layoutInflater.inflate(R.layout.list_item_radio, parent, false)
        return RadioItemHolder(view, this, context)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val radio = radioList[position]

        when (holder.itemViewType) {

            HOLDER_RADIO -> {
                (holder as RadioItemHolder).bindEvent(radio)
            }
        }
    }

    companion object {
        val HOLDER_RADIO = 0
    }

    override fun getItemViewType(position: Int): Int {
        val event = radioList[position]

        return HOLDER_RADIO
    }


    override fun getItemCount(): Int = radioList.size

    override fun onItemSelected(position: Int) {
        viewListener.onItemSelected(position)
    }

    override fun onListButtonClicked(position: Int) {
        viewListener.onListButtonClicked(position)
    }

    override fun onListInterviewCancelButtonClicked(position: Int) {
        viewListener.onListInterviewCancelButtonClicked(position)
    }

    override fun onItemSelectedToEvaluate(position: Int) {
    }

}