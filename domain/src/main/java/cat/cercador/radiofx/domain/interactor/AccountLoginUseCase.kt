package cat.cercador.radiofx.domain.interactor

import cat.cercador.radiofx.domain.callback.LoginCallback
import cat.cercador.radiofx.domain.repository.AccountRepository
import javax.inject.Inject

class AccountLoginUseCase @Inject constructor(private val accountRepository: AccountRepository) : AccountUseCase {

    override fun execute(heroId: Int, callback: AccountUseCase.PostLoginCallback) {
        accountRepository.postLogin(object : LoginCallback.PostLoginCallback {
            override fun onPostLogin() {
                callback.onPostLoginReceived()
            }

            override fun onError() {
                callback.onError()
            }
        })
    }

}