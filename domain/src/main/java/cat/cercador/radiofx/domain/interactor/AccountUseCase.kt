package cat.cercador.radiofx.domain.interactor


interface AccountUseCase {

    interface PostLoginCallback {
        fun onPostLoginReceived()
        fun onError()//TODO include ErrorClass
    }

    fun execute(heroId: Int, callback: PostLoginCallback)
}