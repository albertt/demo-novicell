package cat.cercador.radiofx.domain.callback


interface LoginCallback {

    interface PostLoginCallback{
        fun onPostLogin()
        fun onError()//TODO include ErrorClass
    }
}