package cat.cercador.radiofx.domain.interactor

import cat.cercador.radiofx.domain.callback.RadioCallback
import cat.cercador.radiofx.domain.entity.Radio
import cat.cercador.radiofx.domain.repository.RadioRepository
import javax.inject.Inject

class GetRadioListUseCase @Inject constructor(private val radioRepository: RadioRepository) : RadioUseCase {

    override fun execute(callback: RadioUseCase.GetRadioListCallback) {

        radioRepository.getRadioStations(object : RadioCallback.GetRadioStationsCallback {
            override fun onRadioListReceived(comicList: List<Radio>) {
                callback.onRadioListReceived(comicList)
            }

            override fun onError() {
                callback.onError()
            }
        })
    }


}