package cat.cercador.radiofx.domain.callback

import cat.cercador.radiofx.domain.entity.Radio


interface RadioCallback {

    interface GetRadioStationsCallback{
        fun onRadioListReceived(comicList: List<Radio>)
        fun onError() //TODO include ErrorClass
    }
}