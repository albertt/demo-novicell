package cat.cercador.radiofx.domain.repository

import cat.cercador.radiofx.domain.callback.LoginCallback

interface AccountRepository {

    fun postLogin(callback: LoginCallback.PostLoginCallback)

}