package cat.cercador.radiofx.domain.repository

import cat.cercador.radiofx.domain.callback.RadioCallback


interface RadioRepository {

    fun getRadioStations(callback: RadioCallback.GetRadioStationsCallback)

    fun getRadioStationsRx(heroId: Int)

}