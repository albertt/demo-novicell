package cat.cercador.radiofx.domain.interactor

import cat.cercador.radiofx.domain.entity.Radio


interface RadioUseCase {

    interface GetRadioListCallback {
        fun onRadioListReceived(radioList: List<Radio>)
        fun onError()//TODO include ErrorClass
    }

    fun execute(callback: GetRadioListCallback)
}