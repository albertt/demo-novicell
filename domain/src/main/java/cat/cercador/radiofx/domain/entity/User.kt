package cat.cercador.radiofx.domain.entity


data class User(var token: String? = "",
                var language: Int? = 0,
                var nickname: String? = "",
                var appCode: String? = "")