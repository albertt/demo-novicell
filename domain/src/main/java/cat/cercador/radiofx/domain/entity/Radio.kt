package cat.cercador.radiofx.domain.entity


data class Radio(val id: Int = 0,
                 val name: String = "",
                 val language: String = "",
                 val url: String = "",
                 val active: Boolean = false)