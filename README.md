# Android Clean Architecture base project with Kotlin by Albert Terrones

This is a sample base project making use of the Clean Architecture principles and Kotlin. Objectives:

  - Data, Domain and Presentation independent modules.
  - Use of MVP Architecture in the Presentation Layer.
  - Make use of the Repository Pattern in Data Module.

### Todos

 - MVVM in the Presentation layer with Activity Lifecycle framework.
 - Reactive programming using Kotlin Coroutines
 - Testing

License
----

MIT


**Albert Terrones <aterrones@gmail.com>**

