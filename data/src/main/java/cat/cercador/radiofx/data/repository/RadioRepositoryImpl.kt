package cat.cercador.radiofx.data.repository

import cat.cercador.radiofx.data.repository.datasource.RadioDataStore
import cat.cercador.radiofx.domain.callback.RadioCallback
import cat.cercador.radiofx.domain.entity.Radio
import cat.cercador.radiofx.domain.repository.RadioRepository
import javax.inject.Inject


class RadioRepositoryImpl @Inject constructor(private val radioDataStore: RadioDataStore) : RadioRepository {

    override fun getRadioStations(callback: RadioCallback.GetRadioStationsCallback) {
        radioDataStore.getRadioList(object : RadioDataStore.GetRadioListCallback {
            override fun onRadioListReceived(radioList: List<Radio>) {
                callback.onRadioListReceived(radioList)
            }

            override fun onError() {
                callback.onError()
            }

        })
    }

    override fun getRadioStationsRx(heroId: Int) {

    }
}