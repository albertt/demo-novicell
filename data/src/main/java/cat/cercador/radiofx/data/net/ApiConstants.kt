package cat.cercador.radiofx.data.net


object ApiConstants {

    const val HOSTNAME = "http://radiofx.byte.cat:8080"
    const val ENDPOINT = HOSTNAME + "/api/v1/"

    const val QUERY_PARAM_TS = "ts"
    const val QUERY_PARAM_API_KEY = "apikey"
    const val QUERY_PARAM_HASH = "hash"
    const val TIMEOUT_CONNECTION_VALUE = 60L
    const val TIMEOUT_READ_VALUE = 60L
    const val TIMEOUT_WRITE_VALUE = 60L
    const val PUBLIC_KEY = "711e31e314a5d3888b244b29f5bc8c75"
    const val PRIVATE_KEY = "6753b8699eaef33de2b9586d85de70bd8a9d58bf"

    const val EMAIL = "app@cercador.cat"
    const val PASSWORD = "appcercador123"
}