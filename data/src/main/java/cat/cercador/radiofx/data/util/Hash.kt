package cat.cercador.radiofx.data.util

import android.util.Log
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


object Hash {

    private const val TAG = "Hash"

    fun calculate(algorithm: String, vararg params: String): String {
        var message = ""
        for (param in params) {
            message += param
        }
        try {
            val digest = MessageDigest.getInstance(algorithm)
            digest.update(message.toByteArray())
            val messageDigest = digest.digest()

            val hexString = StringBuilder()
            for (aMessageDigest in messageDigest) {
                var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
                while (h.length < 2) {
                    h = "0$h"
                }
                hexString.append(h)
            }
            return hexString.toString()
        } catch (e: NoSuchAlgorithmException) {
            Log.e(TAG, e.message)
        }

        return ""
    }
}