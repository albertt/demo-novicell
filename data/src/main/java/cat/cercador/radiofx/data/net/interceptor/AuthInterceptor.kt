package cat.cercador.radiofx.data.net.interceptor

import cat.cercador.radiofx.data.net.ApiConstants
import cat.cercador.radiofx.data.util.Hash
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthInterceptor @Inject constructor() : Interceptor {

    companion object {
        const val HASH = "MD5"
        const val TAG = "AuthInterceptor"
    }

    override fun intercept(chain: Interceptor.Chain?): Response {
        var request = chain?.request()
        var url = request?.url()

        val timeStamp = System.currentTimeMillis()

        val urlBuilder = url?.newBuilder()
        urlBuilder?.addEncodedQueryParameter(ApiConstants.QUERY_PARAM_TS, timeStamp.toString())
        urlBuilder?.addEncodedQueryParameter(ApiConstants.QUERY_PARAM_API_KEY, ApiConstants.PUBLIC_KEY)
        urlBuilder?.addEncodedQueryParameter(ApiConstants.QUERY_PARAM_HASH, Hash.calculate(HASH, timeStamp.toString(), ApiConstants.PRIVATE_KEY, ApiConstants.PUBLIC_KEY))

        url = urlBuilder?.build()
        request = request?.newBuilder()?.url(url)?.build()
        println("URl -> " + request?.url())
        return chain?.proceed(request)!!
    }
}