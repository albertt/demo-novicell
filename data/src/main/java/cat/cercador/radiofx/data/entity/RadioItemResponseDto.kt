package cat.cercador.radiofx.data.entity

data class RadioItemResponseDto(var name: String,
                                var language: String,
                                val url: String)