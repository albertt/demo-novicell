package cat.cercador.radiofx.data.entity


class ListBaseResponseDto<T> {

    var offset: Int = 0
    var limit: Int = 0
    var total: Int = 0
    var count: Int = 0
    var results: List<T>? = null
}