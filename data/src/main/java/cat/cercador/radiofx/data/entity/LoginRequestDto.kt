package cat.cercador.radiofx.data.entity

data class LoginRequestDto(val email: String? = "",
                           val password: String? = "")