package cat.cercador.radiofx.data.repository.datasource


interface AccountDataStore {

    interface PostLoginCallback {
        fun onPostLoginReceived()
        fun onError()
    }

    fun postLogin(callback: PostLoginCallback)
}