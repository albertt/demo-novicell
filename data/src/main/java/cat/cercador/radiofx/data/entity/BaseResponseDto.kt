package cat.cercador.radiofx.data.entity


open class BaseResponseDto<T> {

    var code: Int = 0
    var status: String? = null
    var copyright: String? = null
    var attributionText: String? = null
    var attributionHTML: String? = null
    var etag: String? = null
    var data: T? = null
}