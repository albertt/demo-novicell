package cat.cercador.radiofx.data.net

import cat.cercador.radiofx.data.entity.LoginRequestDto
import cat.cercador.radiofx.data.entity.LoginResponseDto
import cat.cercador.radiofx.data.entity.RadioItemResponseDto
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


interface RadioFXApiService {

    @POST("/account/login")
    fun getPublicToken(@Body loginRequestDto: LoginRequestDto) : Call<LoginResponseDto>

    @GET("station")
    fun getRadioList() : Call<List<RadioItemResponseDto>>
}