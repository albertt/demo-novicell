package cat.cercador.radiofx.data.entity.mapper

import cat.cercador.radiofx.data.entity.RadioItemResponseDto
import cat.cercador.radiofx.domain.entity.Radio


object RadioResponseMapper {

    fun toBusinessObject(radioListResponse: List<RadioItemResponseDto>?): List<Radio> {
        val radioList = ArrayList<Radio>()
        if (radioListResponse != null && !radioListResponse.isEmpty()) {
            radioListResponse.forEach {
                radioList.add(RadioItemResponseMapper.toBusinessObject(it))
            }
        }
        return radioList
    }
}