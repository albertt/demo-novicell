package cat.cercador.radiofx.data.entity


data class ImageResponseDto (val path :String = "",
                             val extension :String = "")