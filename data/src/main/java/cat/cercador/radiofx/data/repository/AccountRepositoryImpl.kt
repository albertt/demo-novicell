package cat.cercador.radiofx.data.repository

import cat.cercador.radiofx.data.repository.datasource.AccountDataStore
import cat.cercador.radiofx.domain.callback.LoginCallback
import cat.cercador.radiofx.domain.repository.AccountRepository
import javax.inject.Inject


class AccountRepositoryImpl @Inject constructor(private val accountDataStore: AccountDataStore) : AccountRepository {

    override fun postLogin(callback: LoginCallback.PostLoginCallback) {

        accountDataStore.postLogin(object : AccountDataStore.PostLoginCallback {
            override fun onPostLoginReceived() {
                callback.onPostLogin()
            }

            override fun onError() {
                callback.onError()
            }

        })

    }
}