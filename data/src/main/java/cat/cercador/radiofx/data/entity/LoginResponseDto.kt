package cat.cercador.radiofx.data.entity

data class LoginResponseDto(val user: String? = "",
                            val token: String? = "")