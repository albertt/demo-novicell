package cat.cercador.radiofx.data.repository.datasource

import cat.cercador.radiofx.data.entity.RadioItemResponseDto
import cat.cercador.radiofx.data.entity.mapper.RadioResponseMapper
import cat.cercador.radiofx.data.net.RadioFXApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class RadioDataStoreImpl @Inject constructor(private val radioApiService: RadioFXApiService) : RadioDataStore {

    override fun getRadioList(callback: RadioDataStore.GetRadioListCallback) {
        val call = radioApiService.getRadioList()
        println("URL ->" + call.request().url())

        call.enqueue(object : Callback<List<RadioItemResponseDto>> {
            override fun onFailure(call: Call<List<RadioItemResponseDto>>?, t: Throwable?) {
                callback.onError()
            }

            override fun onResponse(call: Call<List<RadioItemResponseDto>>?, response: Response<List<RadioItemResponseDto>>?) {
                if (response?.code() == 200)
                    callback.onRadioListReceived(RadioResponseMapper.toBusinessObject(response.body()))
                else
                    callback.onError()
            }

        })
    }

}