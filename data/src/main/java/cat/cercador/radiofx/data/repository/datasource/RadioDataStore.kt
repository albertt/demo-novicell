package cat.cercador.radiofx.data.repository.datasource

import cat.cercador.radiofx.domain.entity.Radio


interface RadioDataStore {

    interface GetRadioListCallback {
        fun onRadioListReceived(radioList: List<Radio>)
        fun onError()
    }

    fun getRadioList(callback: GetRadioListCallback)
}