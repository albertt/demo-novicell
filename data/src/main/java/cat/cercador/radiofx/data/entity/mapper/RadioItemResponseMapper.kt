package cat.cercador.radiofx.data.entity.mapper

import cat.cercador.radiofx.data.entity.RadioItemResponseDto
import cat.cercador.radiofx.domain.entity.Radio

object RadioItemResponseMapper {

    fun toBusinessObject(radioItemResponse: RadioItemResponseDto): Radio {
        return Radio(radioItemResponse.hashCode(),
                radioItemResponse.name,
                radioItemResponse.language,
                radioItemResponse.url,
                true)
    }
}