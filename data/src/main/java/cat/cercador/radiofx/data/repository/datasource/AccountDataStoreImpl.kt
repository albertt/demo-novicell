package cat.cercador.radiofx.data.repository.datasource

import cat.cercador.radiofx.data.entity.LoginRequestDto
import cat.cercador.radiofx.data.entity.LoginResponseDto
import cat.cercador.radiofx.data.net.ApiConstants
import cat.cercador.radiofx.data.net.RadioFXApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class AccountDataStoreImpl @Inject constructor(private val radioFXApiService: RadioFXApiService) : AccountDataStore {

    override fun postLogin(callback: AccountDataStore.PostLoginCallback) {
        val call = radioFXApiService.getPublicToken(LoginRequestDto(ApiConstants.EMAIL, ApiConstants.PASSWORD))
        call.enqueue(object : Callback<LoginResponseDto> {
            override fun onFailure(call: Call<LoginResponseDto>?, t: Throwable?) {
                callback.onError()
            }

            override fun onResponse(call: Call<LoginResponseDto>?, response: Response<LoginResponseDto>?) {
                if (response?.code() == 200)
                    callback.onPostLoginReceived()
                else
                    callback.onError()

            }
        })
    }

}